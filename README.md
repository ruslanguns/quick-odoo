# Instalador de Odoo
Version: 11

Con este repositorio recreamos una instanacia completa para Odoo con un comando sin frustraciones y adicionalmente agregamos, el gestor de bases de datos pgAdmin y nginx para su fácil manejo, siéntanse libres de copiar y mejorarlo.

Por temas de seguridad recomiendo ajustar las claves de acceso en el docker-compose.yml

Para instalación de odoo simplemente tengan en cuenta los siguientes requisitos:

##### Requisitos

Correctamente instalado Docker y Docker-Composer en sus versiones más actuales como sea posible. Ya luego buscaré como tener las versiones mínimas y compatibilidades. [Seccion en desarrollo]

##### Instalación

Hacer un git clone al repositorio y correr el siguiente comando:

`docker-compose up -d`

Esto es todo...

[Sección en desarrollo]
Se acepta colaboración para mejorarlo.

##### Thanks to great people:
- [Guidoom](https://gist.github.com/Guidoom/d5db0a76ce669b139271a528a8a2a27f) — Odoo.conf
- [pcuci](https://gist.github.com/pcuci/f44f2608e610170ed23c0a345f89c766) — Docker-compose.yml
- [Odoo](https://www.odoo.com/) — Los más importantes
- [OCA](https://odoo-community.org/) — Por que se lo merecen
- [pgAdmin](https://www.pgadmin.org/) — Excelente gestor de bases de datos Postgres
- [nginx](https://www.nginx.com/) — Virtual Server

##### Sobre mi:

Si deseas contratarme y saber sobre todos mis servicios, te invito a dejarme un mensaje a través de mi sitio web:

[Web de RusGunX](https://rusgunx.tk)